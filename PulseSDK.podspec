Pod::Spec.new do |spec|
spec.name         = "PulseSDK"
spec.version      = "3.16.0"
spec.summary      = "Pulse SDK enables any app to become location aware and deliver in-moment experiences"
spec.homepage     = "https://www.pulseid.com"
spec.author       = "PulseID"
spec.platform     = :ios, '12.0'
spec.source       = { :git => "https://bitbucket.org/pulseid/pulse-ios-release",
                      :tag => "3.16.0"
                    }

spec.default_subspec = 'Core'

spec.subspec 'Core' do |sp|
    sp.vendored_frameworks = 'Framework/PulseCore.framework'
    sp.frameworks = 'CoreLocation', 'CoreTelephony', 'SystemConfiguration', 'WebKit', 'UserNotifications', 'UIKit'
    sp.libraries = 'sqlite3'
    sp.dependency 'CocoaLumberjack'
    sp.dependency 'JSONModel'
    sp.dependency 'JWT'
    sp.dependency 'PulseReactiveC', '3.1.1'
end

spec.subspec 'Motion' do |sp|
    sp.dependency 'PulseSDK/Core'
    sp.vendored_frameworks = 'Framework/PulseMotion.framework'
end

spec.subspec 'IG' do |sp|
    sp.dependency 'PulseSDK/Core'
    sp.vendored_frameworks = 'Framework/PulseIG.framework'
end

end
