Put the following text in your Pod.file test dddd
```plain
platform :ios, '12.0'
target 'Your-target-name' do
    pod 'PulseSDK', :git => 'https://bitbucket.org/pulseid/pulse-ios-release.git', :tag => "X.X"
end
```
Replace the tag value with the latest repository version provided by Pulse.
