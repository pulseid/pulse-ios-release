/*
   Pulse SDK header file
   Copyright (c) 2017 Pulse. All rights reserved.
 */
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PulseRegion.h"

@import CoreLocation;
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif
@protocol PulseSDKDelegate;
/**
 Pulse iOS SDK provides Pulse services for clients.
 */
@interface PulseSDK : NSObject

#pragma mark - Initialisation

/**
 Get the instance of PulseSDK
 */
+ (nonnull instancetype)sharedClient;

#pragma mark - Core

/**
 Sets the Pulse configuration.
 @param config The configuration for Pulse SDK.
 @note config is a NSDictionary object, which needs to include 'url' and 'key'. This function must be called prior to startMonitoring
 */
- (void)setConfig:(nonnull NSDictionary*)config;
/**
 Launch the Pulse SDK.
 @note Call this function in `didFinishLaunchingWithOptions` after `setForegroundMode:backgroundMode`.
 */
- (void)startMonitoring;
/**
 Sets the running mode of SDK
 @param foregroundEnabled Enables SDK in foreground, default: yes
 @param backgroundEnabled Enables SDK in background, default: yes
 */
- (void)setForegroundMode:(BOOL)foregroundEnabled backgroundMode:(BOOL)backgroundEnabled;
/**
 Stops the SDK and location monitoring services
 */
- (void)disable;
/**
 Enables the SDK and location monitoring services.
 This function should be only called after SDK is disabled.
 Requires to relaunch the App.
 */
- (void)enable;
/**
 Gets SDK version
 */
- (nonnull NSString*)getSDKVersion;
/**
 Sets Session ID
 To use the custom session ID, call this method before `startMonitoring` `setSegment`, after `setConfig`
 */
- (BOOL)setSessionId:(nonnull NSString*)identifier;

/**
 Gets the Session ID
 */
- (NSString *)getSessionId;

/* not intented for client */
/**
 :nodoc:
 */
- (void)forceUpdateGeofence:(nonnull NSString*)reason;
/**
 :nodoc:
 */
- (void)updateLocation;

#pragma mark - Notification
/**
 Respond to the click on UILocalNotification
 @param notification Pass UILocalNotfication
 @param foregroundEnabled Enables SDK in foreground
 @param backgroundEnabled Enables SDK in background
 @noted Call this function in `didReceiveLocalNotification`. This function is replaced by `loadViewOnUNNotificationClick` for iOS 10 onwards
 */
- (void)loadViewOnNotificationClick:(nonnull UILocalNotification *)notification startInForeground:(BOOL)foregroundEnabled background:(BOOL)backgroundEnabled NS_DEPRECATED_IOS(4_0, 10_0);
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
/**
 Responds to the click on UNNotification
 @param notification Pass UNNotification
 @param foregroundEnabled Enables SDK in foreground
 @param backgroundEnabled Enables SDK in background
 @noted This function is only available since iOS 10
 */
- (void)loadViewOnUNNotificationClick:(nonnull UNNotification *)notification startInForeground:(BOOL)foregroundEnabled background:(BOOL)backgroundEnabled NS_AVAILABLE_IOS(10_0); //UNUserNotification implementation for ios 10 onwards.
#endif
/**
 Clear notification's property.
 @param propertyName Pass `@"notificatioUrl"` or `@"notificationDict"`
 */
- (void)clearProperty:(nonnull NSString*)propertyName;

#pragma mark - Profile Service

/**
 Sets segments. This method shall be called after `setConfig`.
 The data type in dictionary can only be string.
 @param segementData Pass NSDictionary object
 */
- (void)setSegments:(nonnull NSDictionary*)segementData DEPRECATED_ATTRIBUTE;

/**
 Clears segments.
 */
- (void)clearSegments;

#pragma mark - DFP

/**
 get custom DFP dictionary
 */
- (nonnull NSDictionary*)getDFPTags;

#pragma mark - Location Methods

/**
 Gets user precise location
 The return location is passed in delegate.
 */
- (void)getPreciseLocation;
/**
 Gets user location
 */
- (nonnull NSString*)getPulseUserLocation;

#pragma mark - Conversion Token

/**
 retrieve coversion token based on the lastest event user receives
 @return coversion token. return nil, if user does not interact with any event.
 */
- (nullable NSString*)getConversionToken;

#pragma mark - KYC methods
/**
 geocode an address
 */
- (void)geocode:(nonnull NSString*)address completionHandler:(nonnull void (^)(CLLocationCoordinate2D coord, NSError* _Nonnull error))completionHandler;
/**
 Registers local geofence
 @param coordinate The coordinate of geofence
 @param radius The radius of geofence in meters
 */
- (void)saveLocalGeofenceWithCoord:(CLLocationCoordinate2D)coordinate radius:(int)radius ofType:(enum LocalGeofence)type;
/**
 Delete local geofence
 @param identifier The identifer of geofence
 */
- (void)deleteLocalGeofenceWithIdentifier:(nonnull NSString*)identifier;
/**
 
 Delete All local geofences
 */
- (void)deleteAllLocalGeofence;

/**
 Gets distance between userLocation and location
 @param userLocation The coordinate of user location
 @param location The coordinate of a location
 @return distance in meters
 */
- (float)getDistanceFrom:(nonnull CLLocation*)userLocation to:(nonnull CLLocation*)location;
/**
 Loads Geofences retrieved from Silent Push
 @param data The string of geofences represented in JSON
 */
- (void)loadGeofenceFromGeopush:(nonnull NSString*)data;
/**
 If you specify "permission" as "custom" in the config dictionary,
 call this when you want to request for location permission.
 */
- (void)requestForLocationPermission;
/**
 If you specify "permission" as "custom" in the config dictionary,
 call this when you want to request for notification permission.
 */
- (void)requestForNotificationPermission;

#pragma mark - Region Triggers
/**
 Triggers an enter event for a specified Pulse Region with the user location.
 @param region the region triggers the event
 @param location user location
 */
- (void)enterRegion:(nonnull PulseRegion*)region location:(nullable CLLocation *)location;
/**
 Triggers an exit event for a specified Pulse Region with the user location.
 @param region the region triggers the event
 @param location user location
 */
- (void)exitRegion:(nonnull PulseRegion*)region location:(nullable CLLocation *)location;
#pragma mark - At Venue
/**
 Used for at venue detections. Completion of the operation and its results are
 passed to the atVenue delegate
 */
- (void)atVenue;

#pragma mark - Properties

/**
 @return YES if the app uses customised permission workflow.
 */
@property (nonatomic, readonly) BOOL customisedPermission;
/**
 PulseSDK delegate provides the capability to notify your application.
 */
@property (nonatomic, weak, nullable) id<PulseSDKDelegate> delegate;

@end

/**
 PulseSDK delegate provides the capability to notify your application.
 */
@protocol PulseSDKDelegate <NSObject>
@optional
/**
 delegate method provides a callback on receiving the deep link notification
 */
- (void)didReceiveDeepLinkNotification:(nullable NSString *)notification;

/**
 delegate method provides a callback on getting precise location
 @param location The precise location of user
 @param error Error of getting location update
 */
- (void)didReceivePreciseLocation:(nullable CLLocation*)location error:(nullable NSString*)error;

/**
 Callback triggered when atVenue method is called
 */
- (void)didReceiveVenue:(nullable NSDictionary*)venue error:(nullable NSError*)error userLocation:(nullable CLLocation*)userLocation detectedBeacon:(nullable NSDictionary*)detectedBeacon;
#pragma mark - KYC delegate methods
/**
 delegate method provides a callback on creating local geofence
 @param gID The ID of geofence, nil if not successful
 @param success Whether a geofence has been created successfully
 */
- (void)onLocalGeofenceCreated:(nullable NSString*)gID success:(BOOL)success;
/**
 delegate method provides a callback on deleting local geofence
 @param gID The ID of geofence, nil if not successful
 @param success Whether a geofence has been deleted successfully
 */
- (void)onLocalGeofenceDeleted:(nullable NSString*)gID success:(BOOL)success;
/**
 delegate method provides a callback on entering a local geofence
 @param gID The ID of geofence
 */
- (void)onLocalGeofenceEntered:(nonnull NSString*)gID type:(enum LocalGeofence)type;
/**
 delegate method provides a callback on exiting a local geofence
 @param gID The ID of geofence
 @param dwell The time of staying in a geofence
 */
- (void)onLocalGeofenceExited:(nonnull NSString*)gID type:(enum LocalGeofence)type dwell:(float)dwell;
// end of delegate protocol
@end
