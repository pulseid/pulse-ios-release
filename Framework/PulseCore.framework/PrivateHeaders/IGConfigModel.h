//
//  IGConfigModel.h
//  Core
//
//  Created by Pulse on 23/2/18.
//  Copyright © 2018 com.pulse. All rights reserved.
//

@import JSONModel;

@interface IGConfigModel : JSONModel

@property (nonatomic) NSNumber *outerRegionSize;

- (NSArray<NSNumber *> *)getConvertedLevelAccuracy;

/* six level */
@property (nonatomic, strong) NSArray <NSNumber *> * levelFilter;
@property (nonatomic, strong) NSArray <NSNumber *> * levelTimer;
@property (nonatomic, strong) NSArray <NSNumber *> * levelAccuracy;
/* five circle regions */
@property (nonatomic, strong) NSArray <NSNumber *> * levelRadiusOffset;
/* tolerance is applied only if radius is less than or equal to */
@property (nonatomic) NSNumber *toleranceRadiusThrottle;
/* hit test timeout */
@property (nonatomic, assign) double hitTestTimeout;
/* at venue timeout */
@property (nonatomic, assign) double atVenueTimeout;

/* wait how long a beacon or geofence can be marked as exited. used by beacon for now */
@property (nonatomic, assign) double regionValidForExitTime;
/* when should the atVenue request new location if the data is old */
@property (nonatomic, assign) double atVenueLocationUpdateTime;
/* call the startUpdatingLocation if accuracy value is greater than X */
@property (nonatomic, assign) double atVenueLocationAccuracy;

/**
 determine the maximum IG region radius.
 if it is 0, then deactivates IG.
 */
@property (nonatomic, assign) int triggerRegionSize;

@end
