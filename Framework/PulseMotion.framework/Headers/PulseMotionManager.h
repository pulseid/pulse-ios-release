//
//  PulseMotionManager.h
//  accelerometer
//
//  Created by Pulse on 11/12/17.
//  Copyright © 2017 com.pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreMotion;
@import CoreLocation;

@interface PulseMotionManager : CMMotionManager<CLLocationManagerDelegate>

+ (instancetype)sharedClient;

- (void)startTrackingLocations;
- (void)stopTrackingLocations;
- (void)startDeviceMotions;
- (void)startAccelerometers;
- (void)startPedometers;
- (void)stopPedometers;

@property(strong, nonatomic) CLLocationManager *locationManager;

@end
