//
//  Motion.h
//  Motion
//
//  Created by Pulse on 14/12/17.
//  Copyright © 2017 com.pulse. All rights reserved.
//

#import <UIKit/UIKit.h>
//! Project version number for Motion.
FOUNDATION_EXPORT double MotionVersionNumber;

//! Project version string for Motion.
FOUNDATION_EXPORT const unsigned char MotionVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Motion/PublicHeader.h>

#import "PulseMotionManager.h"
