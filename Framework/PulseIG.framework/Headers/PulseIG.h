//
//  IGLocationManager.h
//  IG
//
//  Created by Pulse on 23/1/18.
//  Copyright © 2018 com.pulse. All rights reserved.
//

/*************
 ENABLES LOCATION UPDATES IN UIBACKGROUND MODES
 *************/

#import <Foundation/Foundation.h>
@import CoreLocation;
@import PulseCore;
@import PulseCore.Private;

typedef NS_ENUM(NSInteger, StatusLevel){
    StatusLevelNaN = 0, // Out of Geofence
    StatusLevel0,
    StatusLevel1,
    StatusLevel2,
    StatusLevel3,
    StatusLevel4  // venue region
};

@interface PulseIG : NSObject<CLLocationManagerDelegate, PulseLocationManagerDelegate>

+ (nullable instancetype) sharedClient;

- (BOOL) validateBackgroundMode;

/**
 Start collecting sensor data and headings.
 */
- (void) startFootprinting;

/**
 Stop collecting sensor data and headings.
 */
- (void) stopFootprinting;

/**
 Determine the user's status
 @param coordinate user location
 @return statusLevel
 */
- (StatusLevel) levelWithCoordinate:(CLLocationCoordinate2D) coordinate;

///MARK: Rings calculations

/**
 Adds ring when a ring is triggered. i.e. enter event, determine state
 */
- (void) addRing:(CLCircularRegion * _Nonnull)region;

- (CLCircularRegion *_Nullable) getRingWithID:(NSString *_Nonnull)identifier;

/**
 Gets the closet ring in rings
 */
- (CLCircularRegion * _Nullable) getClosetRingWithCoordinate:(CLLocationCoordinate2D) coordinate;

/**
 Removes ring with identifier

 @param identifier circular region identifier
 */
- (void) removeRingWithID:(NSString * _Nonnull)identifier;

/**
 Checks equality of CLLocationCoordinate2D
 */
- (BOOL) isEqualCoord:(CLLocationCoordinate2D) coordA withCoord:(CLLocationCoordinate2D) coordB;

/**
 Update the target venue
 */
- (void) updateTargetVenueWithRing:(CLCircularRegion *_Nullable)ring;


/**
 Monitor level circles
 */
- (void) monitorLevelCircle;

/**
 Initialises an array of level circles.

 @return Array of Level Circles
 */
- (NSArray<CLCircularRegion *> * _Nonnull) circleArray;
/**
 initailse a circle

 @param radius of circle
 @return a circle
 */
- (CLCircularRegion * _Nonnull) circleWithRadius:(NSNumber * _Nonnull)radius level:(StatusLevel)level;


/**
 Configuration of IG
 If null, IG is not enabled
 */
@property (nonatomic, strong) IGConfigModel * _Nullable igConfig;

/**
 A Boolean value that indicates whether the service is enabled.
 */
@property (nonatomic, readonly) BOOL enabled;

@property (nonatomic, strong, nonnull) CLLocationManager *locationManager;

@property (nonatomic, assign) StatusLevel userStatus;


///MARK: Smart Logic Properties

/**
 The target venue identifer
 */
@property (nonatomic, strong) NSString * _Nullable venueID;
/**
 The target venue radius
 */
@property (nonatomic, assign) double venueRadius;
/**
 The target venue
 */
@property (nonatomic, assign) CLLocationCoordinate2D venueCoords;

/**
 contains radius offset of each level
 */
@property (nonatomic, strong) NSArray<NSNumber *> * _Nonnull levelRadiusOffset;

/**
 contains radius of each level
 */
@property (nonatomic, strong) NSArray<NSNumber *> * _Nonnull levelRadius;

/**
 contains circle of each level
 */
@property (nonatomic, strong) NSArray<CLCircularRegion *> * _Nonnull levelCircle;
/**
 Client will not be notified of movements of less than the stated value, unless the accuracy has improved.
 */
@property (nonatomic, strong) NSArray<NSString *> * _Nonnull levelIdentifer;


/**
 contains radius of each level
 */
@property (nonatomic, strong) NSArray<NSNumber *> * _Nonnull levelTimer;

/**
 Stops the location updates when a user stays in a level for an amount of time
 */
@property (nonatomic, strong) NSTimer * _Nullable levelExitTimer;


@property (nonatomic) NSInteger toleranceRadiusThrottle;

/**
 an array of outer rings, each ring represents a venue
 append a ring to the array when user enters a level 0 geofence
 remove a ring from the array whenever a user is not within it.
 Supports multiple close-by venues.
 The closet venue is the highest priority target.
 */
@property (nonatomic, strong) NSMutableArray<CLCircularRegion *> * _Nullable rings;

///MARK: WhenInUse

/**
 Stops the hit test
 */
@property (nonatomic, strong) NSTimer * _Nullable hitTestTimer;

/**
 Regions ID that have been checked
 */
@property (nonatomic, strong) NSMutableSet<NSString *> * _Nullable hitTestCheckedRegion;

@end
