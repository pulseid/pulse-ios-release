//
//  IG.h
//  IG
//
//  Created by Pulse on 23/1/18.
//  Copyright © 2018 com.pulse. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IG.
FOUNDATION_EXPORT double IGVersionNumber;

//! Project version string for IG.
FOUNDATION_EXPORT const unsigned char IGVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IG/PublicHeader.h>

#import "PulseIG.h"

